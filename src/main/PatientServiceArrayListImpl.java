package main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.Iterator;

public  class PatientServiceArrayListImpl implements PatientService{
	
	List <Patient> patientlist=new ArrayList();
	
	
		@Override
		public void add(Patient patient) {
			if(patientlist.size()>0 && patientlist.contains(patient))
			{
				System.out.println("PERSON WITH THE DATA ALREADY EXSIST.");
			}
			else
			{

				patientlist.add(patient);
					
			}	
		}


		@Override
		public void display() {

			
			patientlist.forEach(System.out::print);
		
		}
		public void search(String name)
		{
			 patientlist.stream()
		      .filter(str -> str.getName().equals(name))
		      .collect(Collectors.toList());
			
			
			System.out.println(patientlist);
			
			
			
				/*for(Patient search : patientlist) {
					
					if(search.getName().equalsIgnoreCase(name)) {
					System.out.println(search);*/
			      
		}
		
	
		public void sort()
		{
			Collections.sort(patientlist);
			
			display();
		}
		
	
		
		public List<Patient> search(LocalDate date)
		{
			
			return patientlist.stream().filter(date1 -> date1.getDateOfBirth().isAfter(date)).collect(Collectors.toList());
			
		}
		
		public void delete(String name)
		{			
				for(Iterator<Patient> itr = patientlist.iterator();itr.hasNext();) {
				Patient name1=itr.next();	
				if (name1.getName().equals(name))
				{
				itr.remove();
				
				}
				
			}
				System.out.println("PATIENT DELETED SUCESSFULLY");
		}
			
		
		
		public void downloadCSVFile()throws IOException {
			
			
			
				FileWriter file=new FileWriter("download.csv");
				for(Patient pat : patientlist)
				{
					file.write(convertTocsv(pat));
				}
					file.close();
			
		
		}
			 
					
		
		public void downloadJSONFile()throws IOException
		{
			
			
				StringJoiner join = new StringJoiner(",", "[", "]");
				String data;
				FileWriter fileWriter = new FileWriter("downloadJson.json");
				for (Patient patient : patientlist) 
				{
				data = convertTojson(patient);
				join.add(data);
				}
				
				fileWriter.write(join.toString());



				fileWriter.close();
		}
		
	public  int sizeOf()
	{
		return patientlist.size();
	}
		
		public  PatientServiceArrayListImpl(int a)
		{
			
		}


		public Object getCount() {
			// TODO Auto-generated method stub
			return null;
		}
	

				
			}
			
			
			
		
		

	
	


