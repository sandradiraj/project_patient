package main;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/* A interface specifies the operations on Student object
 * @author sandra
 */

public interface PatientService
{
	// this will display all the patient list
	void display();

	/*
	 * This method will add a patient
	 * 
	 * @param add patient  to be added
	 */
	void add(Patient patient);
	
	//will search for a patient using name
	void search(String name);
	//will create a list 
	
	 List<Patient> search(LocalDate dateOfBirth);
	 
	 void delete(String name);
	
 //search using dateOfBirth of birth
	
	//void search1(LocalDate date1);
	void sort();
// will convert details in a csv file	
	default String convertTocsv(Patient patient) 
	{

		StringBuffer ptnt=new StringBuffer();
	 	ptnt.append(patient.getName());
	 	ptnt.append(",");
	 	ptnt.append(patient.getDoctorName());
	 	ptnt.append(",");
	 	ptnt.append(patient.getAge());
	 	ptnt.append(",");
	 	ptnt.append(patient.getGender());
	 	ptnt.append(",");
	 	ptnt.append(patient.getDateOfBirth());
	 	return ptnt.toString();
	 
	}
	void downloadCSVFile()throws IOException;
	default String convertTojson(Patient patient)
	{
		return new StringBuffer().append("{").append("\"Name\":").append(patient.getName()).append("\" ").append("\n").append("\"Doctor Name\":").append("\"")
				.append("\"").append(patient.getDoctorName()).append(",").append("\n").append("\"Age\" :").append(patient.getAge()).append(",").append("\"").append("\n").append("\"Gender\":").append(patient.getGender()).append("\"").append(",")
				.append("\n").append("\n").append("\"").append("\"DOB\":").append(patient.getDateOfBirth()).append("}").append("\n").toString();
		
		
		
		
		
		
	}
	void downloadJSONFile()throws IOException;
}
	
	
	

